<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iglesias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('direccion')->nullable('true');
            $table->string('telefono')->nullable('true');
            $table->string('fax')->nullable('true');
            $table->string('email')->nullable('true');
            $table->string('ciudad');
            $table->string('pais');
            $table->string('activo')->default('true');
            $table->timestamps();
        });

        Schema::create('personas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('telefono');
            $table->string('celular');
            $table->string('genero');
            $table->string('email');
            $table->string('motivo');
            $table->string('alcance');            
            $table->string('oficio')->nullable('true');
            $table->string('pais');
            $table->string('ciudad');

            $table->boolean('material')->default(false);
            $table->boolean('pagado')->default(false);
            $table->boolean('hospedaje')->default(false);
            $table->boolean('voluntario')->default(false);

            $table->string('codigo_registro');
            $table->string('codigo_transaccion');

            $table->string('activo')->default('true');
            $table->string('observacion');

            $table->string('viernes')->nullable('true');
            $table->string('sabado')->nullable('true');
            $table->string('domingo')->nullable('true');

            $table->string('cantidad_parejas')->nullable('true');
            $table->string('cantidad_individuos')->nullable('true');

            $table->integer('id_iglesia')->unsigned();

            $table->foreign('id_iglesia')
                ->references('id')->on('iglesias')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('iglesias');
        Schema::dropIfExists('personas');
        Schema::enableForeignKeyConstraints();
    }
}
