<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">
        
        <title>Inicio Sesión</title>
        
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" 
            href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
            
        <!-- Custom styles for this template -->
        <link href="{{ asset('bootstrap/css/signin.css') }}" rel="stylesheet">
    </head>
    
    <body class="text-center">
        <form class="form-signin" action="{{ route('password.email') }}" method="POST">
            {{ csrf_field() }}

            <img class="mb-4" src="{{ asset('img/ibgracia.png') }}" alt="" height="128">
            <h3 class="h3 mb-3 font-weight-normal">¿Olvidó su contraseña?</h3>
            <p> Introduzca su correo electrónico debajo para re-establecer su contraseña. </p>
            
            @if (session('status'))
                <div class="alert alert-success display-hide" style="display: block;">
                    <button class="close" data-close="alert"></button>
                    <span>{{ session('status') }}</span>
                </div>
            @endif

            @if (count($errors))
                <div class="alert alert-danger display" style="display: block">
                    <button class="close" data-close="alert"></button>
                    @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span><br>
                    @endforeach
                </div>
            @endif

            <label for="inputEmail" class="sr-only">Correo Electrónico</label>
            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Correo Electrónico" required autofocus>
            <div class"checkbox mb-3">    
                <label class="float-center">
                    <a href="{{ route('login') }}" id="forget-password" class="forget-password a-currenttheme">Volver al Login</a>
                </label>
            </div>
            <br>

            <button class="btn btn-lg btn-currenttheme btn-block" type="submit">Recuperar Contraseña</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
        </form>
    </body>
</html>
