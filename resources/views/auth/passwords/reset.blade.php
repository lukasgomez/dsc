<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">
        
        <title>Inicio Sesión</title>
        
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" 
            href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
            
        <!-- Custom styles for this template -->
        <link href="{{ asset('bootstrap/css/signin.css') }}" rel="stylesheet">
    </head>
    
    <body class="text-center">
        <form class="form-signin" action="{{ route('password.request') }}" method="POST">
            {{ csrf_field() }}

            <img class="mb-4" src="{{ asset('img/ibgracia.png') }}" alt="" height="128">
            <h1 class="h3 mb-3 font-weight-normal">Re-establecer Contraseña</h1>
            
            @if (count($errors))
                <div class="alert alert-danger display" style="display: block">
                    <button class="close" data-close="alert"></button>
                    @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span><br>
                    @endforeach
                </div>
            @endif

            <input type="hidden" name="token" value="{{ $token }}">

            <label for="inputEmail" class="sr-only">Correo Electrónico</label>
            <input type="email" id="inputEmail" name="email" class="form-control top-input" placeholder="Correo Electrónico" required autofocus>
            <label for="password" class="sr-only">Contraseña</label>
            <input type="password" id="password" name="password" class="form-control middle-input" placeholder="Contraseña Nueva" required>
            <label for="password_confirmation" class="sr-only">Confirmar Contraseña</label>
            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control button-input" placeholder="Confirmar Contraseña" required>
            <br>
            
            <button class="btn btn-lg btn-currenttheme btn-block" type="submit">Re-establecer Contraseña</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
        </form>
    </body>
</html>
