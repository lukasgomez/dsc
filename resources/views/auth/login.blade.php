<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">
        
        <title>Inicio Sesión</title>
        
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" 
            href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
            
        <!-- Custom styles for this template -->
        <link href="{{ asset('bootstrap/css/signin.css') }}" rel="stylesheet">
    </head>
    
    <body class="text-center">
        <form class="form-signin" action="{{ route('login') }}" method="POST">
            {{ csrf_field() }}

            <img class="mb-4" src="{{ asset('img/ibgracia.png') }}" alt="" height="128">
            <h1 class="h3 mb-3 font-weight-normal">Por favor, inicie sesión</h1>
            
            @if (count($errors))
                <div class="alert alert-danger display" style="display: block">
                    <button class="close" data-close="alert"></button>
                    @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span><br>
                    @endforeach
                </div>
            @endif

            <label for="inputEmail" class="sr-only">Correo Electrónico</label>
            <input type="email" id="inputEmail" name="email" class="form-control top-input" placeholder="Correo Electrónico" required autofocus>
            <label for="inputPassword" class="sr-only">Contraseña</label>
            <input type="password" id="inputPassword" name="password" class="form-control button-input" placeholder="Contraseña" required>
           
            <div class"checkbox mb-3">
                {{-- <label class="float-left">
                    <input type="checkbox" value="remember-me"> Recuérdame
                </label> --}}

                <label class="float-center">
                    <a href="{{ route('password.request') }}" id="forget-password" class="forget-password a-currenttheme">
                        ¿Olvidó su contraseña?</a>
                </label>
            </div>
            
            <button class="btn btn-lg btn-currenttheme btn-block" type="submit">Iniciar Sesión</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2013-2020</p>
        </form>
    </body>
</html>
