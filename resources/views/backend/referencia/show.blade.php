@extends('backend.layout.mainlayout')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('backend.layout.menu')

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Gestor de Referencias</h1>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>Referencias | <small class="text-muted"> Crear nuevas.</small></h4>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ asset('/admin/referencia') }}" autocomplete="off">
                                @csrf

                                @include('backend.layout.partials.errors')

                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control" placeholder="Nombre" name="nombre" value="{{ old('nombre') }}">
                                </div>
        
                                <div class="form-group">
                                    <label for="secuencia">Valor</label>
                                    <input type="text" class="form-control" placeholder="Valor" id="secuencia" name="secuencia" value="{{ old('secuencia') }}" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <label for="activo">Está Activo</label>
                                    <input type="checkbox" id="activo" name="activo" value="true">
                                </div>
        
                                <button type="submit" class="btn btn-primary btn-fill pull-left">Guardar Info</button>
                                <button type="reset" class="btn btn-default btn-fill pull-right">Cancelar</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h4>Lista de Referencias | <small class="text-muted"> Detalle de Referencias.</small></h4>
                        </div>
                        <div class="card-body">
                            @include('backend.referencia.table')
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection

@section('content_custom_js')
@endsection