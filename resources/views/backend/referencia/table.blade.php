<table class="table table-hover">
    <thead>
        <tr class="uppercase">
            <th width="40%">Referencia</th>
            <th width="40%">Valor</th>
            <th width="20%">Estado</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($referencias as $reference)
            <tr onclick="window.location='{{ asset('/admin/referencia/' . $reference->id . '/edit') }}'"
                @if(isset($currentReference) ? $currentReference->id == $reference->id : false) class="blue-mark" @endif>
                <td class="align-middle">{{ $reference->nombre }}</td>
                <td class="align-middle">{{ $reference->secuencia }}</td>
                <td class="align-middle">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" @if($reference->activo) checked="checked" @endif disabled>
                        </div>
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3">No hay referencias registradas.</td>
            </tr>
        @endforelse
    </tbody>
</table>