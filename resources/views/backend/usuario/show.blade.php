@extends('backend.layout.mainlayout')

@section('content')
<div class="container-fluid">
	<div class="row">
		@include('backend.layout.menu')

		<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 class="h2">Gestor de Usuarios</h1>
			</div>

			<div class="row">
				<div class="col-md-4">
					<div class="card ">
						<div class="card-header ">
							<h4>Usuario | <small class="text-muted"> Crear nuevo.</small></h4>
						</div>
						<div class="card-body ">
							<form method="post" action="{{ asset('/admin/usuario') }}" autocomplete="off">
								@csrf
		
								@include('backend.layout.partials.errors')
		
								<div class="form-group">
									<label for="name">Nombre</label>
									<input type="text" class="form-control" placeholder="Nombre" name="name" value="{{ old('name') }}">
								</div>
		
								<div class="form-group">
									<label for="field1">e-Mail</label>
									<input type="email" class="form-control" placeholder="e-Mail" id="field1" name="email" value="{{ old('email') }}" autocomplete="off">
								</div>
		
								<div class="form-group">
									<label for="pasword">Contraseña</label>
									<input type="password" class="form-control" placeholder="Contraseña" id="field2" name="password" autocomplete="off">
								</div>
		
								<div class="form-group">
									<label for="password_confirm">Confirmar Contraseña</label>
									<input type="password" class="form-control" placeholder="Contraseña" name="password_confirm">
								</div>
		
								<div class="form-group">
									<input type="checkbox" name="level" value="true">
									<label for="password_confirm">Es punto de Venta</label>
								</div>
		
								<button type="submit" class="btn btn-primary btn-fill pull-left">Guardar Info</button>
								<button type="reset" class="btn btn-default btn-fill pull-right">Cancelar</button>
								<div class="clearfix"></div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="card ">
						<div class="card-header ">
							<h4>Lista de Usuarios | <small class="text-muted"> Detalle de Usuarios.</small></h4>
						</div>
						<div class="card-body">
							@include('backend.usuario.table')
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
</div>

@section('content_custom_js')
	<script>
		// $(document).ready(function () {
		// 	$('input[type="text"]').attr('disabled', 'disabled');
			
		// 	$('.form-group').on('click', function(e) {
		// 		var $input = $(e.currentTarget).find('input');
		// 		$input.removeAttr('disabled')
		// 		$input.focus();
		// 	});
		// });
	</script>
@endsection