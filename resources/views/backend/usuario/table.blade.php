<table class="table table-hover">
    <thead>
    <tr class="uppercase">
        <th width="40%">Usuario</th>
        <th width="40%">Correo Electrónico</th>
        <th width="20%">Nivel</th>
    </tr>
    </thead>
    <tbody>

    @forelse($usuarios as $key => $user)
        <tr onclick="window.location='{{ asset('/admin/usuario/' . $user->id . '/edit') }}'"
            @if(isset($currentUser) ? $currentUser->id == $user->id : false) class="blue-mark" @endif>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->level }}</td>
        </tr>
    @empty
        <tr>
            <td colspan="3">No hay usuarios registrados.</td>
        </tr>
    @endforelse

    </tbody>
</table>
