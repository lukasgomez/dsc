@extends('backend.layout.mainlayout')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('backend.layout.menu')

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Gestor de Usuarios</h1>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="card ">
                        <div class="card-header ">
                            <h4>Usuario | <small class="text-muted"> Editar usuario.</small></h4>
                        </div>
                        <div class="card-body ">
                            <form id="form" method="post" action="{{ asset('/admin/usuario') . '/' . $currentUser->id }}">
                                @csrf
    
                                @include('backend.layout.partials.errors')
    
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    <input type="text" class="form-control" placeholder="Nombre" name="name"
                                            value="{{ $currentUser->name }}">
                                </div>
    
                                <div class="form-group">
                                    <label for="email">e-Mail</label>
                                    <input type="email" class="form-control" placeholder="e-Mail" name="email"
                                            value="{{ $currentUser->email }}">
                                </div>
    
                                <div class="form-group">
                                    <label for="pasword">Contraseña</label>
                                    <input type="password" class="form-control" placeholder="Contraseña" name="password">
                                </div>
    
                                <div class="form-group">
                                    <label for="password_confirm">Confirmar Contraseña</label>
                                    <input type="password" class="form-control" placeholder="Contraseña"
                                            name="password_confirm">
                                </div>
    
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-8 float-left">
                                            <button id="patchForm" type="button" class="btn btn-info btn-fill">Modificar</button>
                                            <a href="{{ asset('/admin/usuario') }}" class="btn btn-default btn-fill">Cancel</a>
                                        </div>
                                        <div class="col-md-4 float-right">
                                            <button id="destroyForm" type="button" class="btn btn-danger btn-fill pull-right">
                                                Eliminar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card ">
                        <div class="card-header ">
                            <h4>Lista de Usuarios | <small class="text-muted"> Detalle de Usuarios.</small></h4>
                        </div>
                        <div class="card-body table-full-width table-responsive">
                            @include('backend.usuario.table')
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection

@section('page_custom_js')
    {{-- This is for making the form "update/delete" capable --}}
    <script>
        var form = document.getElementById('form');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        document.getElementById('destroyForm').onclick = function () {
            input.value = "DELETE";
            form.appendChild(input);
            form.submit();
        }

    </script>
@endsection