@extends('backend.layout.mainlayout')
 
@section('content')
    <div class="container-fluid">
        <div class="row">
            
            @include('backend.layout.menu')
                    
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Dashboard</h1>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>Estadísticas Conferencia | <small class="text-muted"> Informaciones actuales.</small></h4>
                            </div>
                            <table id="example" class="table table-hover w-100">
                                <thead>
                                    <tr>
                                        <th class="text-left">Dato</th>
                                        <th class="text-center">Valor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Total de personas inscritas: </td>
                                        <td class="text-center">{{ $helper->totalPersonasInscritas() }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total de ayudantes: </td>
                                        <td class="text-center">{{ $helper->totalAyudantes() }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total de mujeres:</td>
                                        <td class="text-center">{{ $helper->totaPersonasPorTipo('genero', 'F') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total de hombres: </td>
                                        <td class="text-center">{{ $helper->totaPersonasPorTipo('genero', 'M') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total de Asistencias Nacionales: </td>
                                        <td class="text-center">{{ $helper->totaPersonasPorTipo('pais', 'República Dominicana') }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total de Asistencias Internacionales:</td>
                                        <td class="text-center">{{ $helper->totaPersonasPorTipo('pais', 'República Dominicana', '!=') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>Estadísticas Conferencia | <small class="text-muted"> Informaciones año pasado.</small></h4>
                            </div>
                            <table id="example" class="table table-hover w-100">
                                <thead>
                                    <tr>
                                        <th class="text-left">Dato</th>
                                        <th class="text-center">Valor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Total de personas inscritas: </td>
                                        <td class="text-center">344</td>
                                    </tr>
                                    <tr>
                                        <td>Total de ayudantes: </td>
                                        <td class="text-center">16</td>
                                    </tr>
                                    <tr>
                                        <td>Total de mujeres:</td>
                                        <td class="text-center">183</td>
                                    </tr>
                                    <tr>
                                        <td>Total de hombres: </td>
                                        <td class="text-center">161</td>
                                    </tr>
                                    <tr>
                                        <td>Total de Asistencias Nacionales: </td>
                                        <td class="text-center">339</td>
                                    </tr>
                                    <tr>
                                        <td>Total de Asistencias Internacionales:</td>
                                        <td class="text-center">5</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <br>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>Desglose de Inscripciones | <small class="text-muted"> Informaciones actuales la plataforma.</small></h4>
                            </div>
                            <table id="example" class="table table-hover w-100">
                                <thead>
                                    <tr>
                                        <th width="80%">Punto de Venta</th>
                                        <th width="20%"># Inscripciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($registros_por_usuario as $usuario)
                                        <tr>
                                            <td>{{ $usuario->name }}</td>
                                            <td class="text-center">{{ $usuario->registrosAsociados()->where('codigo_registro', 'LIKE', $helper->getAnioActual().'%')->count() }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td class="text-center" colspan="2">No hay personas registradas.</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>Descarga de Reportes | <small class="text-muted"> Excel con las informaciones al día.</small></h4>
                            </div>
                            <table id="example" class="table table-hover w-100">
                                <thead>
                                    <tr>
                                        <th width="80%">Reporte</th>
                                        <th width="20%">Descarga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Inscripciones a la fecha</td>
                                        <td>
                                            <a href="{{ route('admin.excel.export') }}" type="button" class="btn btn-sm btn-info btn-fill">Descargar</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
@endsection

@section('content_custom_js')
    <script src="//code.jquery.com/jquery-3.3.1.js"></script>
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
@endsection