<!-- Modal -->
<div class="modal fade" id="participantModal" tabindex="-1" role="dialog" aria-labelledby="participantModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="participantModalLabel">Re-enviar correo de Confirmación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div id="messageStatus" class="d-none">
                    <h3>Correo(s) enviados exitosamente.</h3>
                </div>

                <div id="participantMailingInfo">
                    <input type="hidden" id="participantTransactionCodeInput" value="" />
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="true" id="sendToParticipant" checked>
                        <label class="form-check-label" for="sendToParticipant" id="participantMailLabel">Cargando...</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="true" id="sendToInfo" checked>
                        <label class="form-check-label" for="sendToInfo">secretaria.ibg@gmail.com</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="reloadTableData();">Close</button>
                <button id="sendMessageBtn" type="button" class="btn btn-primary" onclick="sendMessage();">Send Message</button>
            </div>
        </div>
    </div>
</div>

<!-- Maracar Pago Modal -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="paymentModalLabel">Pagar Participante</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div id="showPaymentAction" class="d-none">
                    <input type="hidden" id="payeeId" value="" />
                    <label>¿Está seguro que quiere marcar el participante <span id="payeeName" class="font-weight-bold"></span> como pago?</label>
                    <br />
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="true" id="payDependants" checked>
                        <label class="form-check-label" for="payDependants">Marcar dependientes como pagados  también.</label>
                    </div>
                </div>
                <div id="loadingData">
                    Cargando Data... 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="reloadTableData();">Cerrar</button>
                <button id="markParticipantkAsPaidBtn" type="button" class="btn btn-primary" onclick="markParticipantAsPaid();">Marcar Pago</button>
            </div>
        </div>
    </div>
</div>

<!-- Maracar Voluntario Modal -->
<div class="modal fade" id="volunteerModal" tabindex="-1" role="dialog" aria-labelledby="volunteerModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="volunteerModalLabel">Marcar Voluntario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div id="showVolunteerAction">
                    <input type="hidden" id="volunteerId" value="" />
                    <label>¿Está seguro que quiere marcar el participante <span id="payeeName" class="font-weight-bold"></span> como pago? Este proceso no es reversible.</label>
                </div>
                <div id="loadingDataVolunteer" class="d-none">
                    <h3>Marcado como voluntario exitosamente.</h3>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="reloadTableData();">Cerrar</button>
                <button id="markParticipantAsVolunteerBtn" type="button" class="btn btn-primary" onclick="markParticipantAsVolunteer();">Marcar Voluntario</button>
            </div>
        </div>
    </div>
</div>