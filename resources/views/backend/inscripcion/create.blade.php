@extends('backend.layout.mainlayout')

@section('content')
<div class="container-fluid">
    <div class="row">
        
    @include('backend.layout.menu')

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="{{ asset('img/ibgracia.png') }}" alt="" height="72">
          <h2>Formulario de Inscripción</h2>
          {{-- <p class="lead">Below is an example form built entirely with Bootstrap’s form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p> --}}
        </div>
      
        <div class="row">
            <div class="col-md-12 order-md-1">
                <h4 class="mb-3">Datos Personales</h4>
                <form action="{{ route('saveAndPay') }}" method="POST" novalidate autocomplete="off">
                    @csrf 
                    
                    @include('backend.layout.partials.errors')

                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for="nombres"><b>Nombres:</b></label>
                            <input type="text" class="form-control" id="nombres_0" name="nombres[0][nombre]" placeholder="Nombres [Obligatorio]" value="{{ old('nombres.0.nombre') }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="apellidos"><b>Apellidos:</b></label>
                            <input type="text" class="form-control" id="apellidos_0" name="apellidos[0][apellido]" placeholder="Apellidos [Obligatorio]" value="{{ old('apellidos.0.apellido') }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="sexo"><b>Sexo:</b></label>
                            <select class="custom-select d-block w-100" id="sexo_0" name="sexo[0][sexo]" placelholder="Elija el Sexo [Obligatorio]">
                                <option value="">Elija el Sexo [Obligatorio]</option>
                                <option value="M" @if(old('sexo.0.sexo') == 'M') selected="selected" @endif>Masculino</option>
                                <option value="F" @if(old('sexo.0.sexo') == 'F') selected="selected" @endif>Femenino</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for="telefono"><b>Número de Teléfono:</b></label>
                            <input type="text" class="form-control telefono" id="telefono" name="telefono" placeholder="Teléfono" value="{{ old('telefono') }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="celular"><b>Número de Celular:</b></label>
                            <input type="text" class="form-control telefono" id="celular" name="celular" placeholder="Celular" value="{{ old('celular') }}">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="correo"><b>Correo Electrónico:</b></label>
                            <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo Electrónico" value="{{ old('correo') }}">
                        </div>
                    </div>

                    <div class="row">
                        <div id="bloque_pais" class="col-md-4 mb-3">
                            <label for="pais">País de donde viene:</label>
                            <select class="custom-select d-block w-100" id="pais" name="pais">
                                <option value="">Elija País [Obligatorio]</option>
                                @foreach ($helper->getPaises() as $pais)
                                    <option value="{{ $pais->nombre }}" @if(old('pais') == $pais->nombre) selected="selected" @endif>{{ $pais->nombre }}</option>
                                @endforeach
                                <option value="-1">Otro</option>
                            </select>
                        </div>
                        <div id="bloque_ciudad" class="col-md-4 mb-3 @if(old('pais') != 'República Dominicana') d-none @endif">
                            <label for="ciudad">Ciudad de donde viene:</label>
                            <select class="custom-select d-block w-100" id="ciudad" name="ciudad">
                                <option value="">Elija Ciudad</option>
                                @foreach ($helper->getCiudades() as $ciudad)
                                    <option value="{{ $ciudad->nombre }}" @if(old('ciudad') == $ciudad->nombre) selected="selected" @endif>{{ $ciudad->nombre }}</option>
                                @endforeach
                                <option value="-1">Otra</option>
                            </select>
                        </div>
                        <div class="col-md-4 mb-3">
                        </div>
                    </div>
                
                    <h4 class="mb-3">Datos Acerca de la Iglesia</h4>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="iglesia">Iglesia a la que pertenece:</label>
                            <select class="custom-select d-block w-100" id="iglesia" name="iglesia">
                                <option value="">Elija Iglesia</option>
                                @foreach ($helper->getIglesias() as $iglesia)
                                    <option value="{{ $iglesia->id }}" @if(old('iglesia') === $iglesia->id) selected="selected" @endif>{{ $iglesia->nombre }}</option>
                                @endforeach
                                <option value="-1">Otra Iglesia</option>
                            </select>
                        </div>
                        <div id="bloque_rol" class="col-md-6 mb-3 @if(old('iglesia') !== '0') d-none @endif">
                            <label for="rol">¿Cuál es su papel en la iglesia?</label>
                            <select class="custom-select d-block w-100" id="rol" name="rol">
                                <option value="">Elija Rol</option>
                                @foreach ($helper->getRoles() as $rol)
                                    <option value="{{ $rol->id }}" @if(old('rol') == $rol->id) selected="selected" @endif>{{ $rol->nombre }}</option>
                                @endforeach
                                <option value="-1">Otro</option>
                            </select>
                        </div>
                    </div>
                
                    <h4 class="mb-3">Datos sobre la Conferencia</h4>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="alcance">¿Cómo se enteró de la conferencia?</label>
                            <select class="custom-select d-block w-100" id="alcance" name="alcance">
                                <option value="">Elija una Opción</option>
                                @foreach ($helper->getAlcances() as $alcance)
                                    <option value="{{ $alcance->id }}" @if(old('alcance') == $alcance->id) selected="selected" @endif>{{ $alcance->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="motivo">¿Qué le motivó a asistir la conferencia?</label>
                            <select class="custom-select d-block w-100" id="motivo" name="motivo">
                                <option value="">Elija el Motivo</option>
                                @foreach ($helper->getMotivos() as $motivo)
                                    <option value="{{ $motivo->id }}" @if(old('motivo') == $motivo->id) selected="selected" @endif>{{ $motivo->nombre }}</option>
                                @endforeach
                                <option value="-1">Otro</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="alojamiento">¿Necesita hospedaje? <br />Por favor, agregue las personas que necesitarán alojamiento con usted.</label>
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="check_parejas" name="parejas" value="on" @if(old('parejas') == 'on') checked="checked" @endif>
                                        <label class="custom-control-label" for="check_parejas">Parejas</label>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <input type="text" class="form-control" id="cantidad_parejas" name="cantidad_parejas" placeholder="Cantidad" @if(old('parejas') != 'on') disabled="disabled" @endif value="{{ old('cantidad_parejas') }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="check_individual" name="individual" value="on" @if(old('individual')) checked="checked" @endif>
                                        <label class="custom-control-label" for="check_individual">Individual</label>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <input type="text" class="form-control" id="cantidad_individual" name="cantidad_individual" placeholder="Cantidad" @if(old('individual') != 'on') disabled="disabled" @endif value="{{ old('cantidad_individual') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="alojamiento">¿Cuáles días planea asistir? [Obligatorio]</label>
                                <br />
                                @foreach ($helper->getDiasAsistencia() as $dia)
                                    <input type="checkbox" name="{{ strtolower($dia->nombre) }}" value="{{ true }}" checked> {{ $dia->nombre }}
                                @endforeach
                        </div>
                    </div>

                    <div id="personas_adicionales" @if(count((array)old('nombres')) <= 1) class="d-none" @endif>
                        <h4 class="mb-3">Personas Adicionales</h4>

                        @php $max_value = max(array(count((array)old('nombres')), count((array)old('apellidos')), count((array)old('sexo')))) @endphp

                        @for($i = 1; $i < $max_value; $i++)
                            <div id="persona_adicional_{{ $i }}" class="row mb-3 persona_adicional">
                                <div class="col-md-3 bloque_input">
                                    <label for="nombres[{{ $i }}]">Nombres:</label>
                                    <input type="text" class="form-control" id="nombres[{{ $i }}]" name="nombres[{{ $i }}][nombre]" placeholder="Nombres [Obligatorio]" value="{{ old('nombres.' . $i . '.nombre') }}">
                                </div>
                                <div class="col-md-3 bloque_input">
                                    <label for="apellidos[{{ $i }}]">Apellidos:</label>
                                    <input type="text" class="form-control" id="apellidos[{{ $i }}]" name="apellidos[{{ $i }}][apellido]" placeholder="Apellidos [Obligatorio]" value="{{ old('apellidos.' . $i . '.apellido') }}">
                                </div>
                                <div class="col-md-3 bloque_input">
                                    <label for="sexo[{{ $i }}]">Sexo:</label>
                                    <select class="custom-select d-block w-100" name="sexo[{{ $i }}][sexo]">
                                        <option value="-1" disabled="disabled">Elija el Sexo</option>
                                        <option value="M" @if(old('sexo.' . $i . '.sexo') == 'M') selected="selected" @endif>Masculino</option>
                                        <option value="F" @if(old('sexo.' . $i . '.sexo') == 'F') selected="selected" @endif>Femenino</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="eliminar_persona">Acción:</label>
                                    <a id="{{ $i }}" type="button" class="btn btn-warning form-control eliminar_persona">Eliminar Persona</a>
                                </div>
                            </div>
                        @endfor
                    </div>
                    
                    <hr class="mb-4">
                    <div class="float-right">
                        <a href="#" id="agregar_persona" class="btn btn-info">Agregar Persona</a>
                        <button class="btn btn-primary" type="submit">Proceder con Registro</button>
                    </div>
                </form>
            </div>
    </main>
    </div>
    <br>
</div>

    @include('frontend.inscripcion.modal')
@endsection


@section('content_custom_js')
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    
    <script>
        $('.custom-select').each(function(){
            $(this).children().first().attr('disabled', 'disabled');
        });

        function resetIglesiasSelect() {
            var element = document.getElementById('iglesia');
            element.selectedIndex = 0
        }

        document.getElementById("pais").onchange = function(event) { 
            var element = document.getElementById('bloque_ciudad');

            if(event.target.value == '-1') {
                var input = document.createElement("input");
                input.id = 'pais';
                input.name = 'pais';
                input.className = 'form-control';
                input.placeholder = 'Digite su país de procedencia';
                input.type = "text";
                
                document.getElementById('bloque_pais').removeChild(document.getElementById('pais'));
                document.getElementById('bloque_pais').appendChild(input);
            }

            if(event.target.value === 'República Dominicana') {
                element.classList.remove('d-none');
            } else {
                element.classList.add('d-none');
            }
        }

        document.getElementById('iglesia').onchange = function(event) {
            var element = document.getElementById('bloque_rol');

            if (event.target.value == '-1') {
                document.getElementById('agregar_iglesia').disabled = true;
                $('#iglesiasModal').modal();
                element.classList.add('d-none');
            } else if (event.target.value == '0') {
                element.classList.add('d-none');
            } else {
                element.classList.remove('d-none');
            }
        }

        document.getElementById('check_parejas').onclick = function(event) {
            if (event.target.checked) {
                document.getElementById('cantidad_parejas').disabled = false;
            } else {
                document.getElementById('cantidad_parejas').disabled = true;
            }
        }

        document.getElementById('check_individual').onclick = function(event) {
            if (event.target.checked) {
                document.getElementById('cantidad_individual').disabled = false;
            } else {
                document.getElementById('cantidad_individual').disabled = true;
            }
        }

        document.getElementById('agregar_persona').onclick = function(event) {
            event.preventDefault();

            var bloquePersonaAdicional= document.getElementById('personas_adicionales');
            var classListSize = document.getElementsByClassName('persona_adicional').length;

            if(classListSize == 0) {
                bloquePersonaAdicional.classList.remove('d-none');
            }
            
            var quantity = classListSize + 1;

            var newPersonBlock = document.createElement('div');
            newPersonBlock.id = 'persona_adicional_' + quantity;
            newPersonBlock.className = 'row mb-3 persona_adicional';

            var nameBlock = document.createElement('div');
            nameBlock.className = 'col-md-3 bloque_input';

            var nameLabel = document.createElement('label');
            nameLabel.setAttribute('for', 'nombres[' + quantity + ']');
            nameLabel.innerHTML = 'Nombres:';
            
            var inputNameLabel = document.createElement('input');
            inputNameLabel.type = 'text';
            inputNameLabel.className = 'form-control';
            inputNameLabel.id = 'nombres[' + quantity + ']';
            inputNameLabel.name = 'nombres[' + quantity + '][nombre]';
            inputNameLabel.placeholder = 'Nombres [Obligatorio]';

            nameBlock.appendChild(nameLabel);
            nameBlock.appendChild(inputNameLabel);
            newPersonBlock.appendChild(nameBlock);
            
            var lastNameBlock = document.createElement('div');
            lastNameBlock.className = 'col-md-3 bloque_input';

            var lastNameLabel = document.createElement('label');
            lastNameLabel.setAttribute('for', 'apellidos[' + quantity + ']');
            lastNameLabel.innerHTML = 'Apellidos:';

            var inputLastNameLabel = document.createElement('input');
            inputLastNameLabel.type = 'text';
            inputLastNameLabel.className = 'form-control';
            inputLastNameLabel.id = 'apellidos[' + quantity + ']';
            inputLastNameLabel.name = 'apellidos[' + quantity + '][apellido]';
            inputLastNameLabel.placeholder = 'Apellidos [Obligatorio]';

            lastNameBlock.appendChild(lastNameLabel);
            lastNameBlock.appendChild(inputLastNameLabel);
            newPersonBlock.appendChild(lastNameBlock);

            var genderBlock = document.createElement('div');
            genderBlock.className = 'col-md-3 bloque_input';

            var genderLabel = document.createElement('label');
            genderLabel.setAttribute('for', 'sexo[' + quantity + ']');
            genderLabel.innerHTML = 'Sexo:';

            var genderSelect = document.createElement('select');
            genderSelect.className = 'custom-select d-block w-100';
            genderSelect.placeholder = 'Elija el Sexo';
            genderSelect.name = 'sexo[' + quantity + '][sexo]';
            var optDefecto = document.createElement('option');
            optDefecto.appendChild( document.createTextNode('Elija el Sexo'));
            optDefecto.value = '-1';
            optDefecto.selected = true;
            optDefecto.setAttribute('disabled', 'disabled');
            genderSelect.appendChild(optDefecto);
            var optMasculino = document.createElement('option');
            optMasculino.appendChild( document.createTextNode('Masculino'));
            optMasculino.value = 'M';
            genderSelect.appendChild(optMasculino);
            var optFemenino = document.createElement('option');
            optFemenino.appendChild( document.createTextNode('Femenino'));
            optFemenino.value = 'F';
            genderSelect.appendChild(optFemenino);

            genderBlock.appendChild(genderLabel);
            genderBlock.appendChild(genderSelect);
            newPersonBlock.appendChild(genderBlock);

            var actionBlock = document.createElement('div');
            actionBlock.className = 'col-md-3';

            var actionLabel = document.createElement('label');
            actionLabel.setAttribute('for', 'eliminar_persona');
            actionLabel.innerHTML = 'Acción:';

            var actionLink = document.createElement('a');
            actionLink.id = quantity;
            actionLink.type = 'button';
            actionLink.className = 'btn btn-warning form-control eliminar_persona';
            actionLink.innerHTML = 'Eliminar Persona';

            actionBlock.appendChild(actionLabel);
            actionBlock.appendChild(actionLink);
            newPersonBlock.appendChild(actionBlock);

            bloquePersonaAdicional.appendChild(newPersonBlock);
        }

        document.addEventListener('click', function (event) {
            if (!event.target.matches('.eliminar_persona')) return;
            event.preventDefault();
            
            var id = event.target.id;

            var bloquePersonaAdicional= document.getElementById('personas_adicionales');
            var element = document.querySelector('#persona_adicional_' + id);
            element.parentNode.removeChild(element);

            var classListSize = document.getElementsByClassName('persona_adicional').length;

            if(classListSize == 0) {
                bloquePersonaAdicional.classList.add('d-none');
            }
        }, false);
    </script>
@endsection