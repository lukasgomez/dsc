@extends('backend.layout.mainlayout')

@section('content')
<div class="container-fluid">
    <div class="row">
        
        @include('backend.layout.menu')
        
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Lista de Participantes</h1>
            </div>  
            <table id="datatable" class="table table-striped w-100">
                <thead>
                    <tr>
                        <th class="text-left" width="15%">Nombres</th>
                        <th class="text-left" width="15%">Contacto</th>
                        <th class="text-left" width="25%">Iglesia</th>
                        <th class="text-left" width="15%">Información Registro</th>
                        <th class="text-left" width="10%">Estado</th>
                        <th class="text-left" width="10%">Hospedaje</th>
                        <th class="text-center" width="10%">Acciones</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th class="text-left" width="15%">Nombres</th>
                        <th class="text-left" width="15%">Contacto</th>
                        <th class="text-left" width="25%">Iglesia</th>
                        <th class="text-left" width="15%">Información Registro</th>
                        <th class="text-left" width="10%">Estado</th>
                        <th class="text-left" width="10%">Hospedaje</th>
                        <th class="text-center" width="10%">Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </main>
    </div>
</div>
@include('backend.inscripcion.modals')
@endsection

@section('content_custom_js')
<script src="//code.jquery.com/jquery-3.3.1.js"></script>
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    var raceYear = '{{ \App\Models\Referencia::where('nombre', 'codigo_conferencia_actual')->value('secuencia') }}';
    var url = '{{ asset('/admin/inscripcion') }}';
    var table = '';

    $('#paymentModal').on('hide.bs.modal', function () {
        document.getElementById('loadingData').classList.add('d-none');
        document.getElementById('loadingData').innerHTML = 'Cargando Data...';
        document.getElementById('showPaymentAction').classList.remove('d-none');
        document.getElementById('markParticipantkAsPaidBtn').classList.remove('d-none');
    });

    $('#volunteerModal').on('hide.bs.modal', function () {
        document.getElementById('loadingDataVolunteer').classList.add('d-none');
        document.getElementById('loadingDataVolunteer').innerHTML = 'Cargando Data...';
        document.getElementById('showVolunteerAction').classList.remove('d-none');
        document.getElementById('markParticipantAsVolunteerBtn').classList.remove('d-none');
    })

    function reloadTableData() {
        table.ajax.reload();
    }

    function interactWithModal(modalId, action) {
        jQuery.noConflict();
        $('#' + modalId).modal(action);
    }

    function getParticipantData(url, id) {
        return fetch(url + '/' + id + '/persona', {
            credentials: 'same-origin',
            method: 'GET',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
        })
        .then(response => response.json())
    }

    function setParticipantResendRegistrationInfo(id) {
        document.getElementById('participantMailingInfo').classList.remove('d-none');
        document.getElementById('messageStatus').classList.add('d-none');
        document.getElementById('sendMessageBtn').classList.remove('d-none');

        var participantTransactionCodeInput = document.getElementById('participantTransactionCodeInput');
        var participantMailLabel = document.getElementById('participantMailLabel');

        participantMailLabel.innerHTML = 'Cargando...';
        participantTransactionCodeInput.value = '';

        interactWithModal('participantModal', 'show');

        getParticipantData(url, id)
            .then(data => {
                var participantObj = data;
                participantMailLabel.innerHTML = participantObj.email;
                participantTransactionCodeInput.value = participantObj.codigo_transaccion;
            })
            .catch(error => console.error(error))        
    }

    function sendMessage() {
        document.getElementById('sendToParticipant').disabled = true;
        document.getElementById('sendToInfo').disabled = true;
        document.getElementById('sendMessageBtn').disabled = true;

        var data = {
            codigo_transaccion: document.getElementById('participantTransactionCodeInput').value,
            sendToParticipant: document.getElementById('sendToParticipant').checked,
            sendToInfo: document.getElementById('sendToInfo').checked,
        };
        
        var headers = {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json',
        };

        axios.post('{{ asset('/admin/inscripcion/confirmation/resend') }}',
            data, {
            headers: headers
        })
        .then(function (response) {
            document.getElementById('participantMailingInfo').classList.add('d-none');
            document.getElementById('messageStatus').classList.remove('d-none');
            document.getElementById('sendMessageBtn').classList.add('d-none');

            document.getElementById('sendToParticipant').disabled = false;
            document.getElementById('sendToInfo').disabled = false;
            document.getElementById('sendMessageBtn').disabled = false;                
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    function setParticipantPaid(id) {
        interactWithModal('paymentModal', 'show');
        getParticipantData(url, id)
            .then(data => {
                var participantObj = data;
                document.getElementById('loadingData').classList.add('d-none');
                document.getElementById('showPaymentAction').classList.remove('d-none');
                document.getElementById('payeeName').innerHTML = participantObj.nombres + ' ' + participantObj.apellidos;
                document.getElementById('payeeId').value = participantObj.id;
            })
            .catch(error => console.error(error))    
    }

    function markParticipantAsPaid(id) {
        var data = {
            'id': document.getElementById('payeeId').value,
            'payDependants': document.getElementById('payDependants').checked,
        };

        var headers = {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json',
        };

        document.getElementById('loadingData').classList.remove('d-none');
        document.getElementById('loadingData').innerHTML = 'Aplicando Pago...';
        document.getElementById('markParticipantkAsPaidBtn').classList.add('d-none');
        document.getElementById('showPaymentAction').classList.add('d-none');

        axios.post('{{ asset('/admin/inscripcion/persona/pagada') }}',
            data, {
            headers: headers
        })
        .then(function (response) {
            document.getElementById('loadingData').innerHTML = 'Pago Aplicado!';

        })
        .catch(function (error) {
            console.log(error);
        });
    }

    function setParticipantVolunteer(id) {
        interactWithModal('volunteerModal', 'show');
        getParticipantData(url, id)
            .then(data => {
                var participantObj = data;
                document.getElementById('loadingData').classList.add('d-none');
                document.getElementById('showPaymentAction').classList.remove('d-none');
                document.getElementById('payeeName').innerHTML = participantObj.nombres + ' ' + participantObj.apellidos;
                document.getElementById('volunteerId').value = participantObj.id;
            })
            .catch(error => console.error(error)) 
    }

    function markParticipantAsVolunteer(id) {
        var data = {
            'id': document.getElementById('volunteerId').value,
        };

        var headers = {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Content-Type': 'application/json',
        };

        document.getElementById('loadingDataVolunteer').classList.remove('d-none');
        document.getElementById('loadingDataVolunteer').innerHTML = 'Aplicando Voluntario...';
        document.getElementById('markParticipantAsVolunteerBtn').classList.add('d-none');
        document.getElementById('showVolunteerAction').classList.add('d-none');

        axios.post('{{ asset('/admin/inscripcion/persona/voluntario') }}',
            data, {
            headers: headers
        })
        .then(function (response) {
            document.getElementById('loadingDataVolunteer').innerHTML = 'Voluntario Aplicado!';

        })
        .catch(function (error) {
            console.log(error);
        });
    }

    $(document).ready(function () {
        table = $('#datatable').DataTable({
            ajax: '{{ asset('/admin/inscripcion') }}' + '/' + raceYear + '/list',
            dataSource: 'list',
            columns: [
                { "data": "nombres" },
                { "data": "contact_column_1" },
                { "data": "contact_column_2" },
                { "data": "codigos" },
                { "data": "estado" },
                { "data": "hospedaje" },
                { "data": null,
                    className: 'text-center',
                    sortable: false, 
                    render: function(data, type, full, meta) {
                        return "<div class=\"btn-group mx-auto\" role=\"group\">" +
                            "<a href=\"{{ asset('/admin/inscripcion') . '/' }}" + data.id + "/edit\" class=\"btn btn-info btn-sm align-middle\"><img class=\"white-btn\" src=\"{{ asset('img/edit.svg') }}\" title=\"Modificar Participante\"/></a>" +
                            "<a href=\"#\" class=\"btn btn-info btn-sm align-middle\" onclick=\"setParticipantResendRegistrationInfo(" + data.id + ");\"><img class=\"white-btn\" src=\"{{ asset('img/send.svg') }}\" title=\"Reenviar Correo Confirmación\"/></a>" +
                            "<a href=\"#\" class=\"btn btn-info btn-sm align-middle" + ((data.pagado || data.voluntario) ? ' disabled' : '') + "\" onclick=\"setParticipantPaid(" + data.id + ")\" " + ((data.pagado || data.voluntario) ? ' aria-disabled="true"' : '')  +  "><img class=\"white-btn\" src=\"{{ asset('img/dollar-sign.svg') }}\" title=\"-\"/></a>" +
                            "<a href=\"#\" class=\"btn btn-info btn-sm align-middle" + ((data.voluntario) ? ' disabled' : '') + "\" onclick=\"setParticipantVolunteer(" + data.id + ")\" " + ((data.voluntario) ? ' aria-disabled="true"' : '')  +  "><img class=\"white-btn\" src=\"{{ asset('img/life-buoy.svg') }}\" title=\"-\"/></a>" +
                            "</div>";
                    } 
                }
            ],
            info: true,
            searching: true,
            paging: true,
            sortable: false,
            lengthChange: false,
            processing:true,
            pageLength: 25,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            initComplete: function() {
                // $('.button-test').html('<span data-feather="grid"></span>')
            }
        });
    });
</script>
@endsection