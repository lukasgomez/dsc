<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>DSC | {{ $pageTitle ?? '-' }}</title>
 
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

<!-- Custom styles for this template -->
<link href="{{ asset('bootstrap/css/dashboard.css') }}" rel="stylesheet">
<link href="{{ asset('bootstrap/css/custom.css') }}" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">