<!-- Bootstrap core JavaScript 
    ================================================= 
    Placed at the end of the document so the pages load faster -->
 
<script src="{{ asset('bootstrap/js/jquery-3.2.1.slim.min.js') }}"></script>
    
<script src="{{ asset('bootstrap/js/popper.min.js') }}"></script>

<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

<!-- Graphs -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script>
  // var ctx = document.getElementById("myChart");
  // var myChart = new Chart(ctx, {
  //   type: 'line',
  //   data: {
  //     labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
  //     datasets: [{
  //       data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
  //       lineTension: 0,
  //       backgroundColor: 'transparent',
  //       borderColor: '#007bff',
  //       borderWidth: 4,
  //       pointBackgroundColor: '#007bff'
  //     }]
  //   },
  //   options: {
  //     scales: {
  //       yAxes: [{
  //         ticks: {
  //           beginAtZero: false
  //         }
  //       }]
  //     },
  //     legend: {
  //       display: false,
  //     }
  //   }
  // });
</script>

<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js" data-autoinit="true"></script>
<script src="{{ asset('js/custom.js') }}"></script>

@yield('customjs')
 
 