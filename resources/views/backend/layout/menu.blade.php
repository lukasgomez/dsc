<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.dashboard') === 0) ? 'active' : '' }}" href="{{ route('admin.dashboard.index') }}">
                    <span data-feather="bar-chart-2"></span>
                    Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Operaciones Conferencia</span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column">
           <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.inscripcion.create') === 0) ? 'active' : '' }}" href="{{ route('admin.inscripcion.create') }}">
                    <span data-feather="fast-forward"></span>
                    Registrar Personas <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.inscripcion.index') === 0) ? 'active' : '' }}" href="{{ route('admin.inscripcion.index') }}">
                    <span data-feather="list"></span>
                    Ver Personas
                </a>
            </li>
        </ul>
        
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Administración Webite</span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.page') === 0) ? 'active' : '' }}" href="#">
                    <span data-feather="file-text"></span>
                    Iglesias
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.carousel') === 0) ? 'active' : '' }}" href="#">
                    <span data-feather="box"></span>
                    Roles
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.video') === 0) ? 'active' : '' }}" href="#">
                    <span data-feather="video"></span>
                    Ciudades
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.blog') === 0) ? 'active' : '' }}" href="#">
                    <span data-feather="book"></span>
                    Blog
                </a>
            </li>
            <li class="nav-item d-none">
                <a class="nav-link" href="#">
                    <span data-feather="aperture"></span>
                    Países
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.data') === 0) ? 'active' : '' }}" href="#">
                    <span data-feather="sliders"></span>
                    Motivos
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.referencia') === 0) ? 'active' : '' }}" href="{{ route('admin.referencia.index') }}">
                    <span data-feather="git-branch"></span>
                    Referencias
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ (strpos(Route::currentRouteName(), 'admin.usuario') === 0) ? 'active' : '' }}" href="{{ route('admin.usuario.index') }}">
                    <span data-feather="user"></span>
                    Usuarios
                </a>
            </li>
        </ul>
    </div>
</nav>