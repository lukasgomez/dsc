<!DOCTYPE html>
<html lang="en">
    <head>
        @include('backend.layout.partials.head')
    </head>
    <body>
        {{-- Nav Section --}}
        @include('backend.layout.partials.nav')  
        
        {{-- Header Section --}}
        @include('backend.layout.partials.header')

        {{-- Content Section --}}
        @yield('content')

        {{-- Footer Section --}}
        @include('backend.layout.partials.footer')        

        {{-- Footer Scripts Section --}}
        @include('backend.layout.partials.footer-scripts')
        
        {{-- Content Scripts --}}
        @yield('content_custom_js')
        @yield('content_custom_js_table')
    </body>
</html>