<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">DSC'{{ date('y') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Route::currentRouteNamed('home') ?  'active' : '' }}">
                <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('inscripcion') ?  'active' : '' }}">
                <a class="nav-link" href="{{ route('inscripcion') }}">Inscripción</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Horario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Mapa</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Conferencias Anteriores</a>
            </li>
        </ul>
        <nav class="mt-2 mt-md-0">
            <a href="http://goo.gl/4XAozD" target="_blank" style="margin: 0 10px;color: rgba(255,255,255,.5);"><span data-feather="facebook"></span></a>
            <a href="http://goo.gl/lIQaae" target="_blank" style="margin: 0 10px;color: rgba(255,255,255,.5);"><span data-feather="twitter"></span></a>
            <a href="http://goo.gl/3yplK1" target="_blank" style="margin: 0 10px;color: rgba(255,255,255,.5);"><span data-feather="instagram"></span></a>
            <a href="http://goo.gl/2REkZd" target="_blank" style="margin: 0 10px;color: rgba(255,255,255,.5);"><span data-feather="youtube"></span></a>
            <a href="#" target="_blank"><i class="fa fa-podcast fa-lg"></i></a>
        </nav>
        </div>
    </nav>
</header>