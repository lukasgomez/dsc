<!DOCTYPE html>
<html lang="en">
    <head>
        @include('frontend.layout.partials.head')
    </head>
    <body>        
        {{-- Header Section --}}
        @include('frontend.layout.partials.header')
        
        <main role="main">
        {{-- Content Section --}}
        @yield('content')

        {{-- Footer Section --}}
        @include('frontend.layout.partials.footer')
        </main>      

        {{-- Footer Scripts Section --}}
        @include('frontend.layout.partials.footer-scripts')
        
        {{-- Content Scripts --}}
        @yield('content_custom_js')
        @yield('content_custom_js_table')
        @yield('content_custom_js_modal')
    </body>
</html>