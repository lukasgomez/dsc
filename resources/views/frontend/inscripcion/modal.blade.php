<div class="modal fade" tabindex="-1" role="dialog" id="iglesiasModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar Iglesia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" method="POST" novalidate>
                    @csrf 
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for="nombres"><b>Nombre Iglesia:</b></label>
                            <input type="text" class="form-control" id="modal_nombre_iglesia" placeholder="Nombre Iglesia" onkeyup="validarCamposIglesia()">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="sexo"><b>Ciudad:</b></label>
                            <select class="custom-select d-block w-100" id="modal_ciudad_iglesia" placelholder="Elija la Ciudad" onchange="validarCamposIglesia()">
                                <option value="-1">Elija la Ciudad</option>
                                @foreach ($helper->getCiudades() as $ciudad)
                                <option value="{{ $ciudad->nombre }}">{{ $ciudad->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="apellidos"><b>Teléfono:</b></label>
                            <input type="text" class="form-control telefono" id="modal_telefono_iglesia" placeholder="Teléfono" onkeyup="validarCamposIglesia()">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="agregar_iglesia" type="button" class="btn btn-primary" disabled>Guardar Cambios</button>
                <button id="cerrar_agregar_iglesia" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@section('content_custom_js_modal')
    <script>
        function validarCamposIglesia() {
            var nombre = document.getElementById('modal_nombre_iglesia');
            var ciudad = document.getElementById('modal_ciudad_iglesia');
            var telefono = document.getElementById('modal_telefono_iglesia');
            var btnAgregarIglesia = document.getElementById('agregar_iglesia');

            if (nombre.value.length > 10 && ciudad.value != -1 && telefono.value.length >= 10) { 
                btnAgregarIglesia.disabled = false;
            } else {
                btnAgregarIglesia.disabled = true;
            }
        }

        $('#iglesiasModal').on('hidden.bs.modal', function () {
            resetIglesiasSelect();
        });

        document.getElementById('cerrar_agregar_iglesia').onclick = function(event) {
            resetIglesiasSelect();
        }

        document.getElementById('agregar_iglesia').onclick = function(event) {
            var data = {
                nombre: document.getElementById('modal_nombre_iglesia').value,
                ciudad: document.getElementById('modal_ciudad_iglesia').value,
                telefono: document.getElementById('modal_telefono_iglesia').value,
            };
            
            var headers = {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Content-Type': 'application/json',
            };

            axios.post("{{ asset('api/admin/iglesia') }}",
                data, {
                headers: headers
            })
            .then(function (response) {
                $('#iglesiasModal').modal('hide');
                resetIglesiasSelect();
                location.reload();       
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    </script>    
@endsection