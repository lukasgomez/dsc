@extends('frontend.layout.mainlayout')

@section('content')
<div class="container">
    <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="{{ asset('img/ibgracia.png') }}" alt="" height="72">
      <h3>Detalle de Registro</h3>
      <p>
        {{ \App\Models\Referencia::where('nombre', 'registration_mail_opener')->value('secuencia') }}<br/>
        {!! \App\Models\Referencia::where('nombre', 'registration_mail_dsc_theme')->value('secuencia') !!}
      </p>
  
        <div class="row">
            <div class="col-md-5 order-md-2 mb-4">
                @php $amountPerPerson = \App\Models\Referencia::where('nombre', 'costo_conferencia')->value('secuencia') @endphp
                @php $amountToPay = $amountPerPerson * count($personas->toArray()); @endphp

                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Transacción</th>
                        <th scope="col">Total a Pagar</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><h3>{{ $personas[0]->codigo_transaccion }}</h3></td>
                        <td><h3>RD${{ number_format((float)$amountToPay/100, 2, '.', '') }}</h3></td>
                    </tr>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col text-center">
                        <a href="{{ asset('/') }}" class="btn btn-primary btn-lg btn-block" type="submit">Volver al Inicio</a>
                    </div>
                </div>
            </div>
            <div class="col-md-7 order-md-1">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Tipo de Dato</th>
                        <th scope="col">Dato</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>No. Registro:</td>
                        <td>{{ $personas[0]->codigo_registro }}</td>
                    </tr>
                    <tr>
                        <td>Nombre Completo:</td>
                        <td>{{ $personas[0]->nombres }} {{ $personas[0]->apellidos }}</td>
                    </tr>
                    <tr>
                        <td>Sexo:</td>
                        <td>{{ $personas[0]->genero }}</td>
                    </tr>
                    <tr>
                        <td>e-Mail:</td>
                        <td>{{ $personas[0]->email }}</td>
                    </tr>
                    <tr>
                        <td>Celular:</td>
                        <td>{{ $personas[0]->celular }}</td>
                    </tr>
                    <tr>
                        <td>Teléfono:</td>
                        <td>{{ $personas[0]->telefono }}</td>
                    </tr>
                    </tbody>
                </table>
                <br />

                @if (count($personas->toArray()) > 1)
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" colspan="3">Personas Adicionales en esta transacción:</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($personas as $persona)
                            @if ($loop->first) @continue @endif
                            <tr>
                                <td>{{ $persona->codigo_registro }}</td>
                                <td>{{ $persona->nombres }} {{ $persona->apellidos }}</td>
                                <td>{{ $persona->genero }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection