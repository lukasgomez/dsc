@component('mail::message')

# Hola {{ $nombres }}
{{ \App\Models\Referencia::where('nombre', 'registration_mail_opener')->value('secuencia') }}<br/><br/>
{!! \App\Models\Referencia::where('nombre', 'registration_mail_dsc_theme')->value('secuencia') !!}<br/><br/>

@php $amountPerPerson = \App\Models\Referencia::where('nombre', 'costo_conferencia')->value('secuencia') @endphp
@php $amountToPay = $amountPerPerson * count($adicionales); @endphp

@component('mail::table')
| Tipo de Dato              | Dato                                  |
|---------------------------|:--------------------------------------|
| **No. Transacción:**      | {{ $numero_transaccion }}             |
| **No. Registro:**         | {{ $numero_registro }}                |
| **Nombre Completo:**      | {{ $nombres }} {{ $apellidos }}       |
| **Sexo:**                 | {{ $sexo}}                            |
| **E-Mail:**               | {{ $correo }}                         | 
| **Celular:**              | {{ $celular }}                        |
| **Teléfono:**             | {{ $telefono }}                       |
@endcomponent

@if(count($adicionales) > 1)
@php array_shift($adicionales) @endphp

<div class="table">
    <table>
        <thead>
            <tr>
                <th colspan="3">Personas Adicionales en esta transacción:</th>
            </tr>
        </thead>
        <tbody>
            @foreach($adicionales as $persona)
            <tr>
                <td>{{ $persona['codigo_registro'] }}</td>
                <td>{{ $persona['nombres'] }} {{ $persona['apellidos'] }}</td>
                <td>{{ $persona['genero'] }}</td>
            @endforeach
        </tbody>
    </table>
</div>

@endif

@component('mail::panel')
Total a pagar en esta transacción: **RD${{ number_format((float)$amountToPay/100, 2, '.', '') }}**.
@endcomponent
{{ config('app.name') }}

Atentamente,<br>
@endcomponent
