@component('mail::message')
# ¡Saludos!

Estás recibiendo este correo porque hemos recibido una petición para re-establecer la contraseña de tu cuenta.

@component('mail::button', ['url' => $url])
Reestablecer Contraseña
@endcomponent

Si no solicitaste el cambio de contraseña, no necesitas hacer nada.

Atentamente,<br>
{{ config('app.name') }}
@endcomponent