<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
    $data = array();
    return view('frontend.index', $data);
})->name('home');

Route::get('inscripcion', 'InscripcionController@showFrontEnd')->name('inscripcion');
Route::post('inscripcion/save_and_pay', 'InscripcionController@saveAndPay')->name('saveAndPay');
Route::get('inscripcion/{codigo_persona}/transaccion', 'InscripcionController@transaccion')->name('transaction');
\
Route::get('/mailable', function() {
    $participant = App\Models\Persona::findOrFail(4);
    return (new App\Notifications\RegistroNotification($participant))->toMail($participant);
});

//Move from /home to /admin/dashboard
Route::middleware(['auth'])->group(function() {
    Route::redirect('/home', '/admin/dashboard');
});

Route::middleware(['auth'])->prefix('admin')->name('admin.')->group(function() {
    // Login
    Route::resource('/', 'Auth\LoginController');

    // Dashboard
    Route::resource('dashboard', 'DashboardController');

    // Iglesia
    Route::resource('iglesia', 'IglesiaController');

    // Usuario
    Route::resource('usuario', 'UsuarioController');

    // Referencia
    Route::resource('referencia', 'ReferenciaController');

    // Inscripcion
    Route::resource('inscripcion', 'InscripcionController');
    Route::get('/inscripcion/{anio}/list', 'InscripcionController@list')->name('listaPersonas');
    Route::get('/inscripcion/{id}/persona', 'InscripcionController@showDetail')->name('personaDetail');
    Route::post('/inscripcion/persona/pagada', 'InscripcionController@markAsPaid')->name('personaPagada');
    Route::post('/inscripcion/persona/voluntario', 'InscripcionController@markAsVolunteer')->name('personaVoluntaria');
    Route::post('/inscripcion/confirmation/resend', 'InscripcionController@resendPaymentInformation')->name('resendConfirmation');

    // Import/Export Data
    Route::name('excel.')->group(function () {
        Route::get('export_all', 'ExcelManagerController@exportAll')->name('export');
    });
});
