<?php

namespace App\Http\Controllers;

use App\Models\Persona;
use App\Models\Referencia;
use App\Utilities\Helpers;
use Illuminate\Http\Request;
use App\Models\DiasAsistencia;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Notifications\RegistroNotification;
use Illuminate\Support\Facades\Notification;

class InscripcionController extends Controller
{
    public $data;

    public function __construct() 
    {
        $this->data['pageTitle'] = 'Registro';
        $this->data['helper'] = new Helpers();
        $this->data['dias_asistencia'] = DiasAsistencia::where('activo', true)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fieldValidation(Request $request)
    {
        $rules = [
            'nombres'               => 'required',
            'nombres.*'             => 'required',
            'apellidos'             => 'required',
            'apellidos.*'           => 'required',
            'sexo'                  => 'required',
            'sexo.*'                => 'required',
            'telefono'              => 'required',
            'celular'               => 'required',
            'correo'                => 'required',
            'pais'                  => 'required',
            'ciudad'                => 'required_if:pais,==,República Dominicana',
            'iglesia'               => 'required_if:iglesia,!=,0',
            'rol'                   => 'required',
            'alcance'               => 'required',
            'motivo'                => 'required'
        ];
        
        $customMessages = [
            'nombres.required'               => 'El campo nombre es requerido.',
            'nombres.*.required'             => 'El campo nombre es requerido.',
            'apellidos.required'             => 'El campo apellido es requerido.',
            'apellidos.*.required'           => 'El campo apellido es requerido.',
            'sexo.required'                  => 'El campo sexo es requerido.',
            'sexo.*.required'                => 'El campo sexo es requerido.',
            'telefono.required'              => 'El campo teléfono es requerido.',
            'celular.required'               => 'El campo celular es requerido.',
            'correo.required'                => 'El campo correo es requerido.',
            'pais.required'                  => 'El campo país es requerido.',
            'ciudad.required_if'                => 'El campo ciudad es requerido. ',
            'iglesia.required_if'               => 'El campo iglesia es requerido.',
            'rol.required'                   => 'El campo rol es requerido.',
            'alcance.required'               => 'El campo alcance es requerido.',
            'motivo.required'                => 'El campo motivo es requerido.'
        ];
        
        $this->validate($request, $rules, $customMessages);

        return;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Persona $persona
     */
    public function createPersona(Request $request)
    {
        //TO-DO: Fix Old placement when deleting users. 
        $max_value = max(array(count((array)request()->nombres), count((array)request()->apellidos), count((array)request()->sexo)));

        // Get Transaction Code;
        $transaccionActual = Referencia::BuildNextCode('codigo_transaccion', 'conteo_transaccion')->referencia;
        $personaPrincipal = '';

        //dd($request->all(), $request->input('nombres.'.'0'.'.nombre'), $max_value);

        for($i = 0; $i < $max_value; $i++) {
            $persona = new Persona();
            $persona->nombres = $request->input('nombres.'.$i.'.nombre');
            $persona->apellidos = $request->input('apellidos.'.$i.'.apellido');
            $persona->genero = $request->input('sexo.'.$i.'.sexo');
            $persona->telefono = request()->telefono;
            $persona->celular = request()->celular;
            $persona->email = request()->correo;
            $persona->motivo = request()->motivo;
            $persona->alcance = request()->alcance;
            $persona->oficio = request()->rol;
            $persona->pais = request()->pais;
            $persona->ciudad = request()->ciudad;
            $persona->material = false;
            $persona->pagado = false;
            $persona->hospedaje = (request()->parejas || request()->individual) ?? false;
            $persona->codigo_registro = Referencia::BuildNextCode('codigo_conferencia_actual', 'conteo_conferencia')->referencia;
            $persona->codigo_transaccion = $transaccionActual;
            $persona->viernes = request()->viernes ? true : false;
            $persona->sabado = request()->sábado ? true : false;
            $persona->domingo = request()->domingo ? true : false;
            $persona->cantidad_parejas = request()->cantidad_parejas ?? 0;
            $persona->cantidad_individuos = request()->cantidad_individuos ?? 0;
            $persona->id_iglesia = request()->iglesia;
            $persona->observacion = '';           
            $persona->activo = true;
            $persona->registrado_por = '0';
            $persona->save();

            if ($i == 0) {
                $personaPrincipal = $persona;
            }
        }

        return $personaPrincipal;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.inscripcion.show', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.inscripcion.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->fieldValidation($request);

        if(request()->parejas && request()->parejas == 'on' && empty(request()->cantidad_parejas)) {
            return Redirect::back()->withInput()->withErrors(['El campo cantidad es obligatorio cuando se indica pareja para hospedaje']);
        }

        if(request()->individual && request()->individual == 'on' && empty(request()->cantidad_individual)) {
            return Redirect::back()->withInput()->withErrors(['El campo cantidad es obligatorio cuando se indica individuo/a para hospedaje']);
        }

        $persona = $this->createPersona($request);

        //Send mail
        Notification::route('mail', $persona->email)->notify(new RegistroNotification($persona));

        return redirect(asset('inscripcion/show'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveAndPay(Request $request)
    {
        $this->fieldValidation($request);

        if(request()->parejas && request()->parejas == 'on' && empty(request()->cantidad_parejas)) {
            return Redirect::back()->withInput()->withErrors(['El campo cantidad es obligatorio cuando se indica pareja para hospedaje']);
        }

        if(request()->individual && request()->individual == 'on' && empty(request()->cantidad_individual)) {
            return Redirect::back()->withInput()->withErrors(['El campo cantidad es obligatorio cuando se indica individuo/a para hospedaje']);
        }

        $persona = $this->createPersona($request);

        //Send mail
        Notification::route('mail', $persona->email)->notify(new RegistroNotification($persona));

        return redirect(asset('/inscripcion') .'/' . $persona->codigo_transaccion . '/transaccion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function transaccion($codigo_transaccion)
    {
        $this->data['personas'] = Persona::where('codigo_transaccion', $codigo_transaccion)->orderBy('id')->get();
        return view('frontend.inscripcion.detail', $this->data);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDetail($id)
    {
        $runner = Persona::find($id);
        return response()->json($runner);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['persona'] = Persona::find($id);
        return view('backend.inscripcion.edit', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showFrontEnd()
    {
        return view('frontend.inscripcion.create', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->fieldValidation($request);

        if(request()->parejas && request()->parejas == 'on' && empty(request()->cantidad_parejas)) {
            return Redirect::back()->withInput()->withErrors(['El campo cantidad es obligatorio cuando se indica pareja para hospedaje']);
        }

        if(request()->individual && request()->individual == 'on' && empty(request()->cantidad_individual)) {
            return Redirect::back()->withInput()->withErrors(['El campo cantidad es obligatorio cuando se indica individuo/a para hospedaje']);
        }

        $persona = Persona::find($id);
        $persona->nombres = $request->input('nombres.0.nombre');
        $persona->apellidos = $request->input('apellidos.0.apellido');
        $persona->genero = $request->input('sexo.0.sexo');
        $persona->telefono = request()->telefono;
        $persona->celular = request()->celular;
        $persona->email = request()->correo;
        $persona->motivo = request()->motivo;
        $persona->alcance = request()->alcance;
        $persona->oficio = request()->rol;
        $persona->pais = request()->pais;
        $persona->ciudad = request()->ciudad;
        $persona->material = false;
        $persona->pagado = false;
        $persona->hospedaje = (request()->parejas || request()->individual) ?? false;
        $persona->viernes = request()->viernes ? true : false;
        $persona->sabado = request()->sábado ? true : false;
        $persona->domingo = request()->domingo ? true : false;
        $persona->cantidad_parejas = request()->cantidad_parejas ?? 0;
        $persona->cantidad_individuos = request()->cantidad_individuos ?? 0;
        $persona->id_iglesia = request()->iglesia;
        $persona->observacion = '';           
        $persona->activo = true;
        $persona->registrado_por = Auth::check() ? Auth::user()->id : '0';
        $persona->save();

        return redirect(asset('admin/inscripcion'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return list of personas.
     */
    public function list($anio) 
    {
        $personas = Persona::select(
            'personas.id',
            'personas.pagado', 
            'personas.voluntario',
            DB::raw('CONCAT("<b>Nombres: </b> ",personas.nombres, "<br><b>Apellidos: </b>", personas.apellidos) AS nombres'),
            DB::raw('CONCAT("<b>Celular:</b> ", personas.celular, "<br><b>Teléfono:</b> ", personas.telefono) AS contact_column_1'),
            DB::raw('CONCAT("<b>Correo:</b> ", personas.email, "<br><b>Iglesia:</b> ", iglesias.nombre) AS contact_column_2'),
            DB::raw('CONCAT("<b>Transacción:</b> ", personas.codigo_transaccion, "<br><b>Registro:</b> ", personas.codigo_registro) AS codigos'),
            DB::raw('CONCAT("<b>Pagado:</b> ", CASE WHEN personas.pagado = 1 THEN "SI" ELSE "NO" END, "<br><b>Voluntario:</b> ", CASE WHEN personas.voluntario = 1 THEN "SI" ELSE "NO" END) AS estado'),
            DB::raw('CONCAT("<b>Hospedaje:</b> ", CASE WHEN personas.hospedaje = 1 THEN "SI" ELSE "NO" END, "<br>&nbsp;") AS hospedaje'))
            ->where('personas.codigo_registro', 'LIKE', $anio. '%')
            ->join('iglesias', 'iglesias.id', '=', 'personas.id_iglesia')
            ->orderBy('personas.nombres')
            ->get();

        return response()->json(['aaData' => $personas]);
    }

    public function resendPaymentInformation() 
    {
        $this->validate(request(), [
            'codigo_transaccion'   => 'required',
        ]);
        
        $persona = Persona::where('codigo_transaccion', request()->codigo_transaccion)->orderBy('id')->first();
        
        if(request()->sendToParticipant) {
            Notification::route('mail', $persona->email)->notify(new RegistroNotification($persona));
        }

        if(request()->sendToInfo) {
            Notification::route('mail', 'secretaria.ibg@gmail.com')->notify(new RegistroNotification($persona));
        }
    }

    /**
     * Mark person and dependants as paid.
     */
    public function markAsPaid(Request $request) 
    {
        $this->validate(request(), [
            'id' => 'required'
        ]);

        $persona = Persona::find(request()->id);
        $persona->pagado = true;
        $persona->save();
        
        if (request()->payDependants == true) {
            $personas = Persona::where('codigo_transaccion', $persona->codigo_transaccion)->get();

            foreach($personas as $persona) {
                $persona->pagado = true;
                $persona->save();
            }
        }

        return response()->json([true]);
    }

    /**
     * Mark person as volunteer.
     */
    public function markAsVolunteer(Request $request) 
    {
        $this->validate(request(), [
            'id' => 'required'
        ]);

        $persona = Persona::find(request()->id);
        $persona->voluntario = true;
        $persona->save();

        return response()->json([true]);
    }
}
