<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    public $data;

    public function __construct() {
        $this->data['pageTitle'] = 'Usuarios';
        $this->data['usuarios'] = User::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.usuario.show', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name'             => 'required',
            'email'            => 'email',
            'password'         => 'required',
            'password_confirm' => 'same:password',
        ]);

        $user = new User();
        $user->name = request()->name;
        $user->email = request()->email;
        $user->password = bcrypt(request()->password);
        $user->level = request()->level ? 2 : 1;
        $user->save();

        return redirect(asset('admin/usuario'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['currentUser'] = User::find($id);
        return view('backend.usuario.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'name'             => 'required',
            'email'            => 'email',
            'password_confirm' => 'same:password',
        ]);

        $user = User::find($id);
        $user->name = request()->name;
        $user->email = request()->email;
        $user->password = request()->password ? bcrypt(request()->password) : $user->password;
        $user->level = request()->level ? 2 : 1;
        $user->save();

        return redirect(asset('admin/usuario'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if ($user->level >= 9) {
            return redirect()->back()->withErrors('Este usuario es administrativo y no puede ser eliminado');
        } else {
            $user->delete();
            return redirect(asset('admin/usuario'));
        }
    }
}
