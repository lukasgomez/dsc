<?php

namespace App\Http\Controllers;

use App\Models\Referencia;
use Illuminate\Http\Request;

class ReferenciaController extends Controller
{
    public $data;

    public function __construct() {
        $this->data['pageTitle'] = 'Referencias';
        $this->data['referencias'] = Referencia::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.referencia.show', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'nombre'     => 'required',
            'secuencia'         => 'required',
        ]);

        $referencia = new Referencia();
        $referencia->nombre = request()->nombre;
        $referencia->secuencia = request()->secuencia;
        $referencia->activo = request()->activo ? true : false;
        $referencia->save();

        return redirect(asset('admin/referencia'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['currentReference'] = Referencia::find($id);
        return view('backend.referencia.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'nombre'     => 'required',
            'secuencia'         => 'required',
        ]);

        $referencia = Referencia::find($id);
        $referencia->nombre = request()->nombre;
        $referencia->secuencia = request()->secuencia;
        $referencia->activo = request()->activo ? true : false;
        $referencia->save();

        return redirect(asset('admin/referencia'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $refrencia = Referencia::find($id);

        if(true) {
            return redirect(asset('admin/referencia'));
        } else {
            $refrencia->delete();
            return redirect(asset('admin/referencia'));
        }
    }
}
