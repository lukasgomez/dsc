<?php

namespace App\Http\Controllers;

use App\Models\Persona;
use App\Utilities\Helpers;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Rap2hpoutre\FastExcel\SheetCollection;

class ExcelManagerController extends Controller
{
    public function exportAll() {
        $helper = new Helpers();
        
        $sheets = new SheetCollection([
            $helper->anioActual => Persona::where('codigo_registro', 'LIKE', $helper->anioActual.'%')->get()
        ]);
        
        return (new FastExcel($sheets))->download('ListaCompletaDSC2020.xlsx');
    }
}
