<?php

namespace App\Notifications;

use App\Models\Pais;
use App\Models\Motivo;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RegistroNotification extends Notification
{
    use Queueable;

    public $participant;
    public $amountPaid;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($participant, $amountPaid = 0)
    {
        $this->participant = $participant;
        $this->amountPaid = $amountPaid;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown('mail.registro', 
            [
                'numero_registro'       => $this->participant->codigo_registro,
                'numero_transaccion'    => $this->participant->codigo_transaccion,
                'nombres'               => $this->participant->nombres,
                'apellidos'             => $this->participant->apellidos,
                'sexo'                  => $this->participant->genero,
                'correo'                => $this->participant->email,
                'pais'                  => $this->participant->pais,
                'ciudad'                => $this->participant->ciudad, 
                'motivo'                => Motivo::find($this->participant->motivo)->nombre,
                'celular'               => $this->participant->celular,
                'telefono'              => $this->participant->telefono,
                'viernes'               => $this->participant->viernes,
                'sabado'                => $this->participant->sabado,
                'domingo'               => $this->participant->domingo,
                'adicionales'           => \App\Models\Persona::select('nombres', 'apellidos', 'genero', 'codigo_registro')->where('codigo_transaccion', $this->participant->codigo_transaccion)->orderBy('id')->get()->toArray(),  
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
