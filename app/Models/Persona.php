<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $fillable = ['nombres', 'apellidos','telefono','celular','genero','email','motivo','alcance','oficio','pais','ciudad','material','pagado','hospedaje','codigo_registro','codigo_transaccion','activo','observacion','viernes','sabado','domingo','cantidad_parejas','cantidad_individuos','id_iglesia'];
    protected $table = 'personas';
}
