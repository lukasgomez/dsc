<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiasAsistencia extends Model
{
    protected $fillable = ['nombre', 'activo'];
    protected $table = 'diasAsistencia';
}
