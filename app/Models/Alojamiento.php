<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alojamiento extends Model
{
    protected $fillable = ['nombre', 'activo'];
    protected $table = 'alojamientos';
}
