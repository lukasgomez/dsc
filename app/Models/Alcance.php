<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alcance extends Model
{
    protected $fillable = ['nombre', 'activo'];
    protected $table = 'alcances';
}
