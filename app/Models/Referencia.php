<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Referencia extends Model
{
    protected $fillable = ['referencia', 'secuencia', 'activo'];
    protected $table = 'referencias';

    public function scopeGetNext($query, $referenceName)
    {
        $query->where('referencias.nombre', $referenceName)
            ->update(['referencias.secuencia' => DB::raw('referencias.secuencia + 1')]);

        return $query->select(DB::raw('CONCAT(name, LPAD(secuencia, 4, \'0\')) as referencia'))
            ->where('references.nombre', $referenceName)
            ->first();
    }

    public function scopeBuildNextCode($query, $referenceName, $referenceCounter) 
    {
        $query->where('referencias.nombre', $referenceCounter)
            ->update(['referencias.secuencia' => DB::raw('referencias.secuencia + 1')]);
        
        return $query->select(
                DB::raw('GROUP_CONCAT(
                    CASE 
                        WHEN secuencia regexp \'^[0-9]\' THEN CONCAT(LPAD(secuencia, 4, \'0\')) 
                        ELSE secuencia 
                    END 
                    ORDER BY secuencia DESC SEPARATOR \'\') as referencia')
            )
            ->orWhere('referencias.nombre', $referenceName)
            ->groupBy('activo')
            ->first();

        // SELECT GROUP_CONCAT(
            // CASE 
            //     WHEN `secuencia` regexp '^[0-9]' THEN CONCAT(LPAD(secuencia, 4, '0'))
            //     ELSE `secuencia`
            // END
            // ORDER BY `secuencia` DESC SEPARATOR ''
            // ) as reference
        //     FROM `referencias`
        //     WHERE `referencias`.`nombre` = 'codigo_anio_actual_caminata'
        //     OR `referencias`.`nombre` = 'conteo_caminata'
        //     GROUP BY activo ASC
        
    }

    public function scopeSetPrevious($query, $referenceName)
    {
        $query->where('referencias.nombre', $referenceName)
            ->update(['referencias.secuencia' => DB::raw('referencias.secuencia - 1')]);
    }

}
