<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Motivo extends Model
{
    protected $fillable = ['nombre', 'activo'];
    protected $table = 'motivos';
}
