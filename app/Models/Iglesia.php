<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Iglesia extends Model
{
    protected $fillable = ['nombre', 'direccion', 'telefono', 'fax', 'email', 'ciudad', 'pais', 'activo'];
    protected $table = 'iglesias';
}
