<?php 

namespace App\Utilities;

use App\Models\Rol;
use App\Models\Pais;
use App\Models\Ciudad;
use App\Models\Motivo;
use App\Models\Alcance;
use App\Models\Iglesia;
use App\Models\Persona;
use App\Models\Referencia;
use App\Models\DiasAsistencia;

class Helpers {
    public $anioActual;
    
    public function __construct() 
    {
        $this->anioActual = Referencia::where('nombre', 'codigo_conferencia_actual')->value('secuencia');
    }

    function removeSpecialCharacters($string) {
        $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
            '/[“”«»„]/u'    =>   ' ', // Double quote
            '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
        );

        return preg_replace(array_keys($utf8), array_values($utf8), $string);
    }

    function cleanString($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = str_replace('_', '-', $string); // Replaces all underscore with hyphens.
        $string = $this->removeSpecialCharacters($string);
        $string = strtolower($string); // Set string to lowercase.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    function getAnioActual() {
        return $this->anioActual;
    }

    function getPaises() {
        return Pais::where('activo', true)->get();
    }

    function getCiudades() {
        return Ciudad::where('activo', true)->where('id', '>=', '1')->orderBy('id')->get();
    }

    function getIglesias() {
        return Iglesia::where('activo', true)->orderBy('nombre')->get();
    }

    function getRoles() {
        return Rol::where('activo', true)->orderBy('id')->get();
    }

    function getAlcances() {
        return Alcance::where('activo', true)->orderBy('id')->get();
    }

    function getMotivos() {
        return Motivo::where('activo', true)->orderBy('id')->get();
    }

    function getDiasAsistencia() {
        return DiasAsistencia::where('activo', true)->orderBy('id')->get();
    }

    function totalPersonasInscritas() {
        return Persona::where('codigo_registro', 'LIKE', $this->anioActual.'%')->count();
    }
    
    function totalAyudantes() {
        return Persona::where('codigo_registro', 'LIKE', $this->anioActual.'X%')->count();
    }

    function totaPersonasPorTipo($campo, $valor, $operador = '=') {
        return Persona::where($campo, $operador, $valor)->where('codigo_registro', 'LIKE', $this->anioActual.'%')->count();
    }
}