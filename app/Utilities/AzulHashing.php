<?php

namespace App\Utilities;

class AzulHashing {
    static public $dev_AzulUrl = 'https://pruebas.azul.com.do/PaymentPage/';
    static public $prod_AzulUrl = 'https://pagos.azul.com.do/PaymentPage/';

    static public $dev_MerchantId = '';
    static public $dev_AuthKey = '';

    static public $prod_MerchantId = '';
    static public $prod_AuthKey = '';

    static public $MerchantName = '-';
    static public $MerchantType = 'Sale';
    static public $CurrencyCode = '$';
    static public $Itbis = '000';
    static public $ApprovedUrl = '/registration/approved';
    static public $DeclinedUrl = '/registration/declined';
    static public $CancelUrl = '/registration/cancelled';
    static public $ResponsePostUrl = '';
    static public $UseCustomField1 = '0';
    static public $CustomField1Label = "";
    static public $CustomField1Value = "";
    static public $UseCustomField2 = '0';
    static public $CustomField2Label = "";
    static public $CustomField2Value = "";

    static function generateAuthHashRequest($amount, $order_number)
    {
        $data = [
            'MerchantId'        => AzulHashing::$prod_MerchantId,
            'MerchantName'      => AzulHashing::$MerchantName,
            'MerchantType'      => AzulHashing::$MerchantType,
            'CurrencyCode'      => AzulHashing::$CurrencyCode,
            'OrderNumber'       => $order_number,
            'Amount'            => $amount,
            'Itbis'             => AzulHashing::$Itbis,
            'ApprovedUrl'       => AzulHashing::$ApprovedUrl,
            'DeclinedUrl'       => AzulHashing::$DeclinedUrl,
            'CancelUrl'         => AzulHashing::$CancelUrl,
            'ResponsePostUrl'   => AzulHashing::$ResponsePostUrl,
            'UseCustomField1'   => AzulHashing::$UseCustomField1,
            'CustomField1Label' => AzulHashing::$CustomField1Label,
            'CustomField1Value' => AzulHashing::$CustomField1Value,
            'UseCustomField2'   => AzulHashing::$UseCustomField2,
            'CustomField2Label' => AzulHashing::$CustomField2Label,
            'CustomField2Value' => AzulHashing::$CustomField2Value,
            'AuthKey'           => AzulHashing::$prod_AuthKey
        ];

        $request_string = implode('', $data);
        $request_string = mb_convert_encoding($request_string, 'UTF-16LE', 'ASCII');

        return hash('sha512', $request_string);
    }
}